# OpenHAB Info

Taken from the https://github.com/openhab/openhab-docker

Pre setup an OpenHAB user/group and add yourself too this group

```
sudo groupadd -g 9001 openhab
sudo useradd -u 9001 -g openhab -r -M -s /sbin/nologin -c "OpenHAB system account" openhab
#sudo usermod -a -G openhab ${SUDO_USER:-$USER}
sudo gpasswd -a ${SUDO_USER:-$USER} openhab
sudo chown 9001:9001 addons conf userdata
```

```
docker-compose up -d
```
Visit your docker host IP ie ```http://127.0.0.1:8090``` or if on another device  
like a Raspberry Pi ```192.168.123.30:8090```.  
  
First visit, try ```Demo - Sample Setup```, which will build a sitemap and few other  
items to start with. - https://www.openhab.org/docs/tutorial/1sttimesetup.html  
  
This will take a few minutes to download and setup the ```Demo``` configs.  
 * Basic UI - User interface
 * Rest API - Way to query or set items in your site
 * Paper UI - Part of the Setup system for OpenHAB
 * HABPanel - Panel based interface, nice for LCD/Table use
  
At this point we can look at the installed demo  
```http://192.168.123.30:8090/basicui/app``` locally  
  
or install OpenHAB on your mobile device and setup to point to your  
install ie ```http://192.168.123.30:8090```  
  
