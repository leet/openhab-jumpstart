# Mosquitto Info

Pre setup an Mosquitto user/group

```
sudo groupadd -g 9002 mosquitto
sudo useradd -u 9002 -g mosquitto -r -M -s /sbin/nologin -c "Mosquitto system account" mosquitto
sudo chown 9002:9002 vol
sudo chmod -R g+w,o-r-x vol
```
Don't forget to give yourself access too these folders  
```
sudo gpasss -a $USER mosquitto
```

```
docker-compose up -d
```
  
Great resources:  
 * https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-the-mosquitto-mqtt-messaging-broker-on-ubuntu-18-04
 * https://obrienlabs.net/how-to-setup-your-own-mqtt-broker/
