# OpenHAB functioning intro

Why automation? Home or otherwise … Be informed, have more control, make things more efficient, let the computers do things that they are good at, like doing the same thing over and over again, with you doing “just a little” setup.

Our goal will be to setup a basic OpenHAB install. Setup some sensor feedback and control something, like a pool pump or lights.

Aimed at docker-compose install, should cover Raspberry Pi 3+ (below might need native packages and less feature set), Windows, Mac and Linux systems that have Docker CE and docker-compose installed and functioning.

Why OpenHAB? I know and use OpenHAB … Reviewing automation tools in 2015 for a talk, OpenHAB 1.x was Free/Open Source, able to run on R-Pi 1 and had more interfaces than any of the other automation tools at the time and I was able to get a basic running OpenHAB system run for the talk.Present day there are many more options and are way more mature than back in the day, with many more features. Node-RED, Home Assistant, etc - https://ubidots.com/blog/open-source-home-automation/

OpenHAB is by no means perfect, but has an active and vibrant community. Current shortcoming I hope will be worked on in the near future, is multi-user granular and flexible access management and control with 3rd party integration like SAML or OAuth. https://github.com/eclipse/smarthome/issues/579 Looks promising, but has not had activity in some time?

Following setup is not optimised for speed or light, but I have tried to avoid “vendor/product” lock-in, ie OpenHAB has a mqtt broken built in, but is not as customizable as the Mosquitto package set. OpenHAB also has RRD persistent data storage, but is not as flexible nor can’t be accessed outside of OpenHAB, unlike influxDB. Same for graphs and alerts.

Breaking out these components, adds the ability to scale vertically, or deploy different parts open multiple servers or locations, ie multiple R-Pi.


OpenHAB 2.5
items, sitemap and rules
HTTP webhooks
Mqtt interface

https://myopenhab.org/login vs public instance vs vpn/reverse proxy
Android App requires one of the above

    Basic Graphs

Mqtt - collect and interface 
    Mosquitto (install and test)

TICK stack (install and test)
Telegraf - agent
Influxdb - basics (need investigate retention) 
Chronograf - Graphs
Kapacitor - Alerting

Telegram - Group setup and bot interface 

Setup sensors and relays, etc.


We can use any IoT devices that support mqtt, webhooks and/or that has OpenHAB Bindings - https://www.openhab.org/addons/

I suggest using open hardware or something hackable with an Open Source firmware like many of the Sonoff devices or build your own devices using either the ESP8266 or ESP32 devices.

Prefered FOSS firmware:
https://github.com/letscontrolit/ESPEasy
https://github.com/xoseperez/espurna
https://github.com/arendst/Tasmota

Another solution, is build or using a bridge/gateway type device
https://www.home-assistant.io/integrations/rflink/

Or possible SDR on the R-Pi - https://photobyte.org/using-the-raspberry-pi-as-an-rtl-sdr-dongle-server/
