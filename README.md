# openhab-jumpstart  
  
OpenHAB 2.5 - Introduction and Jumpstart and helpful/useful bits  
  
This repo and documentation are not meant to be exhaustive nor  
complete information for setting up an OpenHAB installation.  
The intention is to provide a simple working setup for a  
quick setup, to evaluate OpenHAB in a real-world environment  
using external data or IoT devices. Some information on why  
I choose a setup and how to get it working.  
  
[Work in Porgress]  
Feedback and/or Merge Requests welcome.  

