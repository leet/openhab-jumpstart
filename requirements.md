## Requirements for project  
  
Docker - https://docs.docker.com/get-docker/  
  
Docker Compose - https://docs.docker.com/compose/install/  
  
Tested on OSX with Docker CE Desktop and  
 Raspberry Pi 3 with Hypriot image - https://blog.hypriot.com/  
  
  
Test your setup by running ```docker-compose up -d``` in ```tests/01_hello-world```  
  
  
Helpful read - https://www.linode.com/docs/applications/containers/how-to-use-docker-compose/  
  


